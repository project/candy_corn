<?php

function phptemplate_search_theme_form($form) {
  return _phptemplate_callback('search-box', array('form' => $form));
}

function candy_corn_primary_links() {
  $links = menu_primary_links();
  if ($links) {
    $output .= '<ul>';
    foreach ($links as $link) {

      $output .= '<li>' . $link . '</li>';
    }; 
    $output .= '</ul>';
  }
  return $output;
}

function candy_corn_secondary_links() {
  $links = menu_secondary_links();
  if ($links) {
    $output .= '<ul>';
    foreach ($links as $link) {

      $output .= '<li>' . $link . '</li>';
    }; 
    $output .= '</ul>';
  }
  return $output;
}