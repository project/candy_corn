<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language ?>" xml:lang="<?php print $language ?>">

<head>
  <title><?php print $head_title ?></title>
  <?php print $head ?>
  <?php print $styles ?>
  <script type="text/javascript"><?php /* Needed to avoid Flash of Unstyle Content in IE */ ?> </script>
</head>

<body>

  <div id="banner">
    <div class="top_links clearfix" id="topnav">
      <?php if (isset($secondary_links)) { ?><?php print candy_corn_secondary_links() ?><?php } ?>
    </div>
    <?php if ($logo) { ?><a href="<?php print $base_path ?>" title="<?php print t('Home') ?>"><img src="<?php print $logo ?>" alt="<?php print t('Home') ?>" /></a><?php } ?>
    <div class="page_title"><?php if ($site_name) { ?><a id="page_title" href="<?php print $base_path ?>" title="<?php print t('Home') ?>"><?php print $site_name ?></a><?php } ?><br />
    <?php if ($site_slogan) { ?><?php print $site_slogan ?><?php } ?></div>
  </div>


	
  <?php if ($sidebar_left) { ?>
    <div class="leftcontent" id="nav">
      <div id="left_bg_top">&nbsp;</div>
      <?php if (isset($primary_links)) { ?><?php print candy_corn_primary_links() ?><?php } ?>
	  <div class="left_news">
      <p>&nbsp;</p>
      <?php print $sidebar_left ?>
      </div>
    </div>
  <?php } ?>

  <div id="centercontent">
    <div><?php print $header ?></div>
    <div class="pumpkin"></div><div id="news_title_blk"><?php print $title ?></div>
	<div class="tabs"><?php print $tabs ?></div>
    <?php print $help ?>
    <?php print $messages ?>
    <?php print $content; ?>
    <br />
  </div>

  <?php if ($sidebar_right) { ?>
    <div id="rightcontent">
      <div class="right_news">
        <?php print $search_box ?>
		<br />
        <?php print $sidebar_right ?>
      </div>
    </div>
  <?php } ?>
	
  <div id="footer">
    <?php print $footer_message ?>
  </div>
  <?php print $closure ?>
</body>
</html>